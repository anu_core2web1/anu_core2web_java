class SDemo2{
        void methodFun(){
                System.out.println("In fun method");
        }
        void methodGun(){
                System.out.println("In gun method");
        }
        void methodRun(){
                System.out.println("In run method");
        }
        public static void main(String[] args){
                methodFun();
                methodGun();
                methodRun();
        }
}
