import java.util.Scanner;
class Io1{

	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);
		System.out.print("Enter num: ");
		int num=sc.nextInt();
		if(num%2==1){
			System.out.println(num+ " is an odd number");
		}else{
			System.out.println(num+" is an even number");
		}
	}
}
