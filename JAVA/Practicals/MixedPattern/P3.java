import java.util.*;
class P3{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number of rows: ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){
			int ch = row+64;
			int num=1;
			for(int j = 1;j<=row;j++){

				if(i%2==1){
					System.out.print((char)ch+" ");
					ch--;
				}else{
					System.out.print(num +" ");
					num++;
				}
			}
			System.out.println();
		}
	}
}
