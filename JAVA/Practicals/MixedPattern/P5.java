import java.util.*;
class P5{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter Rows: ");
		int row = sc.nextInt();

		int num = row;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				System.out.print(i*j +" ");
			}
			System.out.println();
		}
	}
}
