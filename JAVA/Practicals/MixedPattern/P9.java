import java.util.*;
class P9{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Rows: ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){
			int ch =65+row-i;

			for(int j=1;j<=row-i+1;j++){
				if(i%2==1){
					System.out.print(j +" ");
				}else{
					System.out.print((char)ch+" ");
					ch--;
				}
			}
				System.out.println();
			
		}
	}
}
