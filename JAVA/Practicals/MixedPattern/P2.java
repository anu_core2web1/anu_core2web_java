import java.util.*;
class P2{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number of rows: ");
		int row = sc.nextInt();

		int num=row;
		int ch=row+64;

		for(int i = 1;i<=row;i++){
			for(int j=1;j<=row;j++){
				System.out.print((char)ch +""+ num + " ");
				num--;
			}
			if(row%2==1){
			        num+=4;
			}
			else{
				num+=5;
			}
			System.out.println();
		}
	}
}
