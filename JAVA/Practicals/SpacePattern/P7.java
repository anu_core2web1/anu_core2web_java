import java.util.*;
class P7{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Rows: ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){
			int num=1;
			for(int sp=1;sp<i;sp++){
				System.out.print("\t");
			}
			for(int j=1;j<=i;j++){
				System.out.print(num+"\t");
				num++;
			}
			System.out.println();
		}
	}
}


