import java.util.*;

class P10{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter row: ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){
			int ch=64+i;
				for(int j=1;j<=row-i+1;j++){
					if(row%2==0){
			
		         			if((i%2==1 && j%2==0) || (i%2==0 && j%2==1)){
						System.out.print((char)ch+" ");
			        		}else{
				  		System.out.print(ch+" ");
				        	}
					}
					else{
		         			if((i%2==1 && j%2==0) || (i%2==0 && j%2==1)){
						System.out.print(ch+" ");
			        		}else{
				  		System.out.print((char)ch+" ");
				        	}
					}

					ch++;
				}
			System.out.println();
		}
	}
}
