import java.util.*;
class P8{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows: ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){
			for(int sp=1;sp<i;sp++){
				System.out.print("\t");
			}
			for(int j=i;j<=row;j++){
				System.out.print(j+"\t");
			}
			System.out.println();
		}
	}
}
