class Program8{
       public static void main(String[] args){

	       double percentage=85.00;
	       if(percentage>=70 && percentage<=100){
		       System.out.println("Passed: first class with distinction");
	       }else if(percentage>=60 && percentage<=69.99){
		       System.out.println("Passed: first class");
	       }else if(percentage>=50 && percentage<=59.99){
		        System.out.println("Passed: second class");
	       }else if(percentage>=35 && percentage<=49.99){
		       System.out.println("Pass");
	       }else{
		       System.out.println("Fail");
	       }
       }
}
