import java.util.*;
class P6{
	public static void main(String[] args){

		Scanner sc = new Scanner (System.in);
		System.out.print("Enter size:");
		int size = sc.nextInt();
		
		System.out.println("Enter characters: ");
		char arr[] = new char[size];

		
		for(int i=0;i<arr.length;i++){
			arr[i] = sc.next().charAt(0);
		}
		System.out.println("Output: ");
		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]);
        	}
	}
}
