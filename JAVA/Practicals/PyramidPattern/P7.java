import java.util.*;
class P7{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows: ");
		int row = sc.nextInt();

		for(int i =1;i<=row;i++){

			int ch=64+i;
			for(int sp=row;sp>i;sp--){
				System.out.print("\t");
			}
			for(int j=1;j<=i*2-1;j++){
				if(i%2==1){
				System.out.print(i +"\t");
			}else{
				System.out.print((char)ch +"\t");
		       	}
		}
			System.out.println();
		}
		
	}
}
