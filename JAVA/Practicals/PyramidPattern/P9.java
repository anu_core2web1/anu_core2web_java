import java.util.*;
class P9{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows: ");
		int row = sc.nextInt();

		for(int i =1;i<=row;i++){

			char c='A';
			char ch='a';
			for(int sp=row;sp>i;sp--){
				System.out.print("\t");
			}
			for(int j=1;j<=i*2-1;j++){
				if(i%2==1){
					if(j<i){
				            System.out.print(c++ +"\t");
					}else{
					    System.out.print(c-- +"\t");
					}
				}else{
			         	if(j<i){
				        	System.out.print(ch++ +"\t");
			        	}else{
				              System.out.print(ch-- +"\t");
		         	}
		        }
	        }
			System.out.println();
		}
		
	}
}
