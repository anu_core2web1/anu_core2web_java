import java.io.*;
class P9{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter number of Rows: ");
		int row=Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++){
			int num=1;
			
			for(int j=1;j<=row-i+1;j++){
				
				System.out.print(num +" ");

				num++;
			}
			System.out.println();
			
		}
		
	}
}
