import java.io.*;

class TP3{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter rows: ");
		int row=Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++){
			int ch=row+64;
			for(int j=1;j<=i;j++){

				System.out.print((char)ch +" ");
				ch--;
			}
			System.out.println();
		}
	}
}

