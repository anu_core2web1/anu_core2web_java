import java.io.*;
class TP1{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter number of Rows: ");
		int row=Integer.parseInt(br.readLine());

		System.out.print("Enter number: ");
		int num=Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++){
			
			for(int j=1;j<=i;j++){
				
				System.out.print(num +" ");
			}
			System.out.println();
		}
		
	}
}
