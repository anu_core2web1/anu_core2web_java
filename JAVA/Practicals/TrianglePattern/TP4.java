import java.io.*;

class TP4{

        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter rows: ");
		int row=Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++){
			int ch=row+96;
			int c=row+64;
			for(int j=1;j<=i;j++){

				if(i%2==1){
					System.out.print((char)ch+ " ");
					ch--;
				}else{
					System.out.print((char)c+ " ");
					c--;
				}
			}
			System.out.println();
		}
	}
}
					


