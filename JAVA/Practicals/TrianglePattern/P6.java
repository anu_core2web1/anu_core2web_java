import java.io.*;
class P6{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter number of Rows: ");
		int row=Integer.parseInt(br.readLine());

		int num=1;
		for(int i=1;i<=row;i++){
                        	
			for(int j=1;j<=i;j++){
				
				System.out.print(num*j +" ");

			}
			System.out.println();
			num++;
		}
		
	}
}
