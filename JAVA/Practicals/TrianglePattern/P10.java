import java.io.*;

class P10{

	public static void main(String[] arr)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter number of rows: ");
		int row=Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++){
			int num =i;
			for(int j=1;j<=row-i+1;j++){

				System.out.print(num +" ");
				num++;
			}
			System.out.println();
		}
	}
}



