import java.util.*;

class P1{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows: ");
		int row =sc.nextInt();
		int num=1;
		for(int i=1;i<=row;i++){
			for(int sp=1;sp<=row-1;sp++){
				System.out.print(" "+"\t");
			}
			for(int j=row;j>=row-1;j--){
				System.out.print(num +"\t");
				num+=2;
			}
			System.out.println();
		}
	}
}
