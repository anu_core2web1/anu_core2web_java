class P10{

	public static void main(String[] args){

		int sum=0;
		int prod=1;
		int num=9367924;
		while(num>0){
			int rem=num%10;
			if(rem%2==1){
				sum=sum+rem;
			}
			else{
				prod=prod*rem;
			}
			num/=10;
		}
		System.out.println("Sum of even digits: "+sum);
		System.out.println("Product of odd digits: "+prod);
	}
}

