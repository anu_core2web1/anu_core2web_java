class P9{

	public static void main(String[] args){

		int sum=0;
		int num=2469185;
		while(num>0){
			int rem=num%10;
			if(rem%2==1){
				int sq=rem*rem;
				sum=sum+sq;
			}
			num/=10;
		}
		System.out.println(sum);
	}
}
